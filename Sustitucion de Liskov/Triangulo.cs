﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sustitucion_de_Liskov
{
    class Triangulo
    {
        public void Proceso(Calcular calculo)
        {
            double result = calculo.Proceso();
            Console.WriteLine("Resultado=" + result);
        }
    }
}
