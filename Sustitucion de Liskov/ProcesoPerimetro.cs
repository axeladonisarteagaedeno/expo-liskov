﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sustitucion_de_Liskov
{
    class ProcesoPerimetro:Calcular
    {
        public override double Proceso()
        {
            double result = (this.Base + this.Lado_A+this.Lado_B);
            return result;
        }
    }
}
