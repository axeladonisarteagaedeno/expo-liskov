﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sustitucion_de_Liskov
{
    class ProcesoArea : Calcular
    {
        public override double Proceso()
        {
            double result = (this.Base* this.Altura)/2;
            return result;
        }
    }
}
